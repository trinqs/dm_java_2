import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        if(liste.size()==0){
            return null;
        }
        Integer min=liste.get(0);
        for (int i=1; i<liste.size();i++){
            if (liste.get(i)<min){
                min=liste.get(i);
            }
        }
        return min;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for(T elem:liste){
            if(elem.compareTo(valeur)>1){
                return false;
            }
        }
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> res= new ArrayList<>();
        for(T elem:liste1){
            if (liste2.contains(elem) ){
                if(!res.contains(elem)){
                    res.add(elem);
                }
            }
        }
        return res;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> res= new ArrayList<>();
        if (texte.length()==0){
            res.add("");
            return res;
        }
        String mot="";
        for(int i=0; i<(texte.length());i++){
            if(String.valueOf(texte.charAt(i))==" "){
                if(mot.length()>0){
                    res.add(mot);
                }
                mot="";
            }
            else{
                mot+=texte.charAt(i);
            }
        }
        if(mot.length()>0){
            res.add(mot);
        }
        return res;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        Map<String, Integer> dico_Mots = new HashMap<>();
        List<String> listeMot = decoupe(texte);

        for ( String mot : listeMot){
            if (dico_Mots.containsKey(mot)) {
                dico_Mots.put(mot, dico_Mots.get(mot) + 1);
            }
            else {
                dico_Mots.put(mot, 1);
            }
        }

        if (dico_Mots.size()==0) {
            return null;
        }

        String valeurMax = listeMot.get(0);
        Collections.sort(listeMot);

        for (String mot : listeMot){
            if (dico_Mots.get(valeurMax) < dico_Mots.get(mot)){
                valeurMax = mot;
            }
            if (dico_Mots.get(mot).equals(dico_Mots.get(valeurMax))){ 
                List<String> listemax = new ArrayList<>();
                listemax.add(mot);
                listemax.add(valeurMax);
                valeurMax = Collections.min(listemax);
            }
        }
        return valeurMax;
    }
    
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int p_ouverte=0;
        int p_ferme=0;
        for(int i=0;i<chaine.length();i++){
            if(String.valueOf(chaine.charAt(i))=="("){
                p_ouverte++;
            }
            else if(String.valueOf(chaine.charAt(i))==")"){
                p_ferme++;
            }
            if(p_ferme>p_ouverte){
                return false;
            }
        }
        if(!(p_ouverte==p_ferme)){
            return false;
        }
        return true;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
     // ( - ) - [
   // [ - ( - ]

    public static boolean bienParentheseeCrochets(String chaine){
        int p_ouverte=0;
        int p_ferme=0;
        int c_ouverte=0;
        int c_ferme=0;
        for(int i=0;i<chaine.length();i++){
            if(String.valueOf(chaine.charAt(i))=="("){
                p_ouverte++;
            }
            else if(String.valueOf(chaine.charAt(i))==")"){
                p_ferme++;
            }
            else if(String.valueOf(chaine.charAt(i))=="["){
                c_ouverte++;
            }
            else if(String.valueOf(chaine.charAt(i))=="]"){
                c_ferme++;
            }
            if(p_ferme>p_ouverte || c_ferme>c_ouverte){
                return false;
            }
        }
        if(p_ouverte==p_ferme && c_ouverte==c_ferme){
        return true;
        }
        return false;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        if(liste.size()==0){
            return false;
        }
        else{
        boolean res=false;
        int indicedebut=0;
        if (liste.get(indicedebut)==valeur){
                return true;
            }
        int indicefin=liste.size()-1;
        if (liste.get(indicefin)==valeur){
                return true;
            }
        while(indicefin-indicedebut>1){
            int indicemilieu=indicedebut+indicefin;
            if (liste.get(indicemilieu)==valeur){
                return true;
            }
            else if(liste.get(indicemilieu)<valeur){
                indicefin=indicemilieu;
            }
            else{
                indicedebut=indicemilieu;
            }
        }
        return res;
        }
    }



}
